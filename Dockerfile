FROM node
RUN npm install express@4.16.1
RUN npm install ip@1.1.5
COPY app.js .
COPY package.json .
CMD ["npm","start"]
EXPOSE 3000
